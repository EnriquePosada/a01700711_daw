<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/hola', function (Request $request, Response $response, array $args) {
    // Sample log message
    //$this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return 'Hola';
});
