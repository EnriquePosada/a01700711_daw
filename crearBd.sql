SET DATEFORMAT dmy

sp_help cfentreganclave Entregan


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')

DROP TABLE Materiales 

CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')

DROP TABLE Proveedores

CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')

DROP TABLE Proyectos

CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')

DROP TABLE Entregan

CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
)


BULK INSERT a1700711.a1700711.[Materiales]
   FROM 'e:\wwwroot\a1700711\materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
BULK INSERT a1700711.a1700711.[Proyectos] 
  FROM 'e:\wwwroot\a1700711\Proyectos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1700711.a1700711.[Proveedores] 
  FROM 'e:\wwwroot\a1700711\Proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 


BULK INSERT a1700711.a1700711.[Entregan] 
  FROM 'e:\wwwroot\a1700711\Entregan.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  )

INSERT INTO Materiales values(1000, 'xxx', 1000) 
Delete from Materiales where Clave=1000 and Costo=1000

ALTER Table Materiales add constraint llaveMateriales PRIMARY KEY (Clave)
insert into Materiales values(1000,'xxx',1000)

alter table Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
alter table Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
--Para hacer una llave primaria compuesta para la tabla de ENTREGAN
alter table Entregan add constraint llaveEntregan PRIMARY KEY (Clave, Numero, RFC, Fecha)

--select * from Materiales


insert into Entregan values(0,'xxx',0,'1-jan-02',0);
delete from Entregan where Clave = 0;


alter table Entregan add constraint cfentreganclave
foreign key (Clave) references Materiales(Clave);
--cf...=clave foranea

alter table Entregan add constraint cfentreganrfc   
foreign key (RFC) references Proveedores(RFC);

alter table Entregan add constraint cfentregannumero   
foreign key (Numero) references Proyectos(Numero);

insert into Entregan values(1000,'AAAA800101',5000,GETDATE(),0);
delete from Entregan where Clave = 1000;
alter table Entregan add constraint cantidad check(cantidad > 0);
insert into Entregan values(1000,'AAAA800101',5000,GETDATE(),-20)
delete from Entregan where Clave = 1000;

