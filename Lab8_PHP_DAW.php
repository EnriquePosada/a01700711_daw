<!DOCTYPE html>
<html>
<head>
	<title>Laboratorio #9: Introduccion a PHP</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<div class="jumbotron">
	<h1>Laboratorio #9: Introduccion a <i>PHP</i></h1>
	</div>
</head>
<style type="text/css">h1{text-align: center;}</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<body>
	<ul>
		<li>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</li>
			<ul>
				<li>La funcion <i>phpinfo()</i> sirve para mostrar la informacion sobre la configuracion de <i>PHP</i>. Algunas de las cosas que me llaman la atencion es que si insertas un parametro entonces te mostrara la configuracion de lo que se puso como parametro. Otra cosa que me llamo la atencion es muestra el estado actual de <i>PHP</i> e incluye mucha informacion acerca del servidor y del mismo <i>PHP</i>. Finalmente, otra cosa que me llamo la atencion es que asi como existe phpinfo(), tambien existe phpversion(), php credits() y otros para obtener mas informacion.</li>
			</ul>
		<li>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</li>
			<ul>
				<li>EL ambiente de produccion es el ambiente puesto por el servidor en donde la pagina web estara corriendo para consumo externo (otras personas). De alli en fuera estaria bien leer sbre el ambiente de produccion ya que aqui se explica que necesitas para montar la pagina a un servidor y publicarla para que otros usuarios puedan acceder a ella para lo que se requiera. </li>
			</ul>
		<li>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</li>
			<ul>
				<li>Un usuario realiza una peticion al servidor con algun servidor, por lo cual se procesa el codigo que se tiene y realiza las conecciones que se tienen que hacer y despues de este procedimiento se regresa upor ejemplo una pagina web que visualice infrormacion relevante para el usuario. </li>
			</ul>

	</ul>
<hr>
<?php 

 	function average($arreglo){
 		echo "<ol><li>El promedio del arreglo de numeros es de: " . (array_sum($arreglo)/count($arreglo))."</li>\n";
 	}

 	function median($arreglo){
 		Arsort($arreglo);//Sorting del arreglo que entro como parametro
 		$keys = array_keys($arreglo);
 		echo "<li>La mediana del arreglo es: " . $keys[round(count($keys)/2)] . "</li>\n";
 	}

 	function listaitems($arreglo){
 		echo "<li><ul>";
 		echo "<li>El promedio del arreglo de numeros es de: ".array_sum($arreglo)/count($arreglo)."</li>";
 		Arsort($arreglo);//Sorting del arreglo que entro como parametro
 		$keys = array_keys($arreglo);
 		//El plan original no fue exitoso debido a que se llama primero la funcion antes de poder insertarlo en el echo. 
 		//$hellobeautiful=median($arreglo);//Increible que se pueda asignar la funcion a una variable la cual me pueda permitir llamar al arreglo
 		echo "<li>La mediana del arreglo es de: ".$keys[round(count($keys)/2)]/*$hellobeautiful*/."</li>";
 			echo "<li>Sort menor a mayor</li><ul>";
		for ($i=0; $i < count($arreglo); $i++) { 
			echo "<li>".$arreglo[$i]."</li>";
		}
			echo "</ul>";

			echo "<li>Sort mayor a menor</li><ul>";
		for ($i=count($arreglo)-1; $i > 0; $i--) { 
			echo "<li>".$arreglo[$i]."</li>";
		}
			echo "</ul>";
		echo "</ul></li>";
 	}

 	function printtablecuadradoscubos($arreglo){
		$sizearray=count($arreglo);

		echo "<li><table>";
		echo "<thead> <td>Numero</td>  <td>Elevado al Cuadrado</td>  <td>Elevado al Cubo</td></thead>";

		for ($i=0; $i < $sizearray; $i++) { 
			echo '<tr>';
			//echo "<td></td>";
			echo "<td>$arreglo[$i]</td>";
			echo "<td>".pow($arreglo[$i], 2)."</td>";
			echo "<td>".pow($arreglo[$i], 3)."</td>";
			echo '</tr>';
		}
		echo "</table></li>";

 	}

 	function problemalibre(){
 		echo "<li>Prueba de Encriptacion de Contraseñas: (Asumo que es importante para el projecto y para projectos personales en el futuro) \n";
 		echo "<br>La contraseña es: <b><i>HelloWorld!</b></i><br>A continuacion se usa <i><u>password_hash('String', PASSWORD_DEFAULT)</i></u> para hacer un hash que permita crear una nueva contraseña (fuerte/segura) para proteger la informacion. <b>HelloWorld!</b> ahora es: ";
 		$password=password_hash("HelloWorld!", PASSWORD_DEFAULT);
 		echo "<b>".$password."</b>";
 		echo "<br>Si le dieron <i>refresh</i>, observaran que cada vez el hash es distinto y esto ayuda para la proteccion de contraseñas. <br>";
 		echo "</li></ol>";
    	if (password_verify('HelloWorld!', $password)) {
    		echo 'Muy bien, contraseña aceptada. Esta linea aparece porque la contraseña encriptada que fue recibida es en realidad la contraseña, por lo tanto password valido.';
		} else {
    		echo 'Contraseña invalida';
			}
	

 	}
	//echo "Hello World for you my man";
 	$myarray=array(1,2,3,4,5,6,7,8,9,10,16,25,36,49,64);
 	//echo "Arreglo: ".$myarray."\n";
 	average($myarray); //Succesful
 	echo "<br>";//Estos espacios son para que pueda experimentar con las funciones de php sin que lo <br> que estaban dentro de las funciones afecten la visualizacion del resultado.
 	//*Note_to_Self: Siempre se tiene que pasar como parametro el arreglo ya que PHP no parece aceptar variables globales
 	median($myarray);//Succesful...
 	echo "<br>";
 	listaitems($myarray);//Succesful
 	printtablecuadradoscubos($myarray);//Succesful
 	problemalibre();//Succesful
/*
Una función que reciba un arreglo de números y devuelva su promedio  ...Done...

Una función que reciba un arreglo de números y devuelva su mediana ..Done...

Una función que reciba un arreglo de números y muestre la lista de números, y como ítems de una lista html muestre el promedio, la media, y el arreglo ordenado de menor a mayor, y posteriormente de mayor a menor

Una función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n

Escoge algún problema que hayas implementado en otro lenguaje de programación, y dale una solución en php, en una vista agradable para el usuario.
*/

 ?>
</body>
</html>


