<!DOCTYPE html>
<html>
<head>
	<title>Lab #11: Manejo de Formas con PHP</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<div class="jumbotron">
	<h1>Lab #11: Manejo de Formas con <i>PHP</i> y Modelo de Capas</h1>
	</div>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<style type="text/css">
	.error{
		color: red;
	}
	.forma{
		text-align: center;
	}
</style>
<?php include("Lab11_PHP.php"); ?>
<body>
	<div class="forma">
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

		<label>Nombre Usuario: </label><br><input type="text" name="user">
		<span class="error">* <?php echo $usernameErr;?></span>
		<br><br>
		<label> Contraseña:</label><br>
		<input type="password" name="password">
		<span class="error">* <?php echo $passwordErr;?></span>
		<br><br>
		<button type="submit" class="btn btn-secondary" data-toggle="modal" data-target="">Iniciar <i>Sesion</i></button>	

	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Crear <i>Cuenta</i></button>

	</form>
	</div>




	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Hola, Crea tu Cuenta</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">


	        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

	          <div class="form-group">
	            <label for="recipient-name" class="form-control-label">Nombre Usuario</label>
	            <input type="text" class="form-control" id="recipient-name" name="username"> <span class="error"> * <?php echo $usernameErr;?></span>
	          </div>
	          <div class="form-group">
	          <label>Contraseña</label><br>
	            <input type="password" for="message-text" class="form-control-label" id="passwd" name="userpassword">
	            <span class="error"> * <?php echo $passwordErr; ?> </span>
	          </div>
	          <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary" onclick="bienvenido()">Crear Cuenta</button>
	      </div>
	        </form>


	      </div>
	      
	    </div>
	  </div>
	</div>




		
	<div id="accordion">
	  	<div class="card">
	    	<div class="card-header" id="headingOne">
	     		 <h5 class="mb-0">
	        		<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</button></h5>
	</div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
			<ul>
				<li>as</li>
			</ul>
			</div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          ¿Cuál es la diferencia entre una variable de sesión y una cookie?
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">

		<ul>
		<li>
			sd
		</li>
		</ul>
      
</div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          ¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
	<ul>
		<li>
			
		</li>
	</ul>      
	</div>
    </div>
  </div>
</div>
<div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          ¿Qué es CSRF y cómo puede prevenirse?
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
	<ul>
		<li></li>
			
	</ul>      
	</div>
    </div>
  </div>
</div>

<br><br>

</body>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</html>