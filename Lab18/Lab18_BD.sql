SET DATEFORMAT dmy


/*
Materiales(Clave, Descripción, Costo, PorcentajeImpuesto) 
Proveedores(RFC, RazonSocial) 
Proyectos(Numero, Denominacion) 
Entregan(Clave, RFC, Numero, Fecha, Cantidad) 
*/

--Insertar registros
--INSERT INTO Materiales VALUES (0001, 'Vibranio', 600000 , 80) ; 
--INSERT INTO Materiales VALUES (0006, 'MateriaInventada2', 400000 , 80) ; 
--INSERT INTO Materiales VALUES (0003, 'Materia Oscura', 900000 , 80) ; 
--INSERT INTO Materiales VALUES (0004, 'Adamantium', 5000000 , 80) ; 
--INSERT INTO Materiales VALUES (0007, 'MateriaInventada5000', 400000 , 80) ; 

--delete from Materiales where clave=6


--Primera Parte

--1era de primera parte
/*
select sum(cantidad) as 'SumaCantidades', sum((costo*(1+(porcentajeimpuesto/100)))) as 'ImporteTotal' 
from entregan e, materiales m
where m.clave=e.clave and fecha between '01/01/1997' and '31/12/1997'
*/

--2da de primera parte
/*
select razonsocial, sum(cantidad) as 'CantidadEntregas', sum (cantidad*(costo*(1+(porcentajeimpuesto/100)))) as 'ImporteTotal'
from entregan e, proveedores p, materiales m
where e.clave=m.clave and e.rfc=p.rfc
group by razonsocial
*/

--3era de primera parte
/*
select m.clave, descripcion, sum(cantidad) as 'CantidadTotal', min(cantidad) as 'CantidadMinima', max(cantidad) as 'CantidadMaxima', sum (cantidad*(costo*(1+(porcentajeimpuesto/100)))) as 'ImporteTotal'
from materiales m, entregan e
where m.clave=e.clave
group by m.clave, descripcion
having avg(cantidad)>400
*/

--4ta de primera parte 
/*
select razonsocial, avg(cantidad) as 'CantidadPromedioMaterialEntregado', m.clave, descripcion
from proveedores p, entregan e, materiales m
where p.rfc=e.rfc and m.clave=e.clave 
group by razonsocial, m.clave, descripcion
having avg(cantidad) > 500
*/

--5ta de primera parte (Requiere 4)
/*
select razonsocial, avg(cantidad) as 'CantidadPromedioMaterialEntregado', m.clave, descripcion
from proveedores p, entregan e, materiales m
where p.rfc=e.rfc and m.clave=e.clave 
group by razonsocial, m.clave, descripcion
having avg(cantidad) < 370 or avg(cantidad) > 450 
order by avg(cantidad)
*/


--Segunda Parte

--1era segunda parte  (no hay materiales en la tabla que no hayan sido entregados) 
/*
select m.clave, descripcion
from materiales m
where m.clave not in (select clave from entregan)
*/

--2da de segunda parte 
/*
select razonsocial
from proveedores p, entregan e, proyectos pr
where p.rfc=e.rfc and pr.numero=e.numero and (pr.denominacion='Queretaro limpio' and p.razonsocial in (select distinct razonsocial
from proveedores p, entregan e, proyectos pr
where p.rfc=e.rfc and pr.numero=e.numero and (pr.denominacion='Vamos Mexico')
group by denominacion, razonsocial)) 
group by denominacion, razonsocial
*/

--3era de segunda parte
/*
select descripcion
from materiales m
where m.clave not in (select e.clave from entregan e, proyectos p where p.numero=e.numero and denominacion='CIT Yucatan')
*/

--4ta de segunda parte
/*
select razonSocial, avg(cantidad) as 'CantidadPromedioEntregada'
from entregan e, proveedores p
where p.rfc=e.rfc
group by razonSocial 
having avg(cantidad) > (select avg(cantidad) as 'CantidadPromedioRFCVAGO'
from entregan e
where rfc='VAGO780901'
)
*/

--5ta de segunda parte
/*
select p.rfc, razonsocial
from proveedores p, entregan e, proyectos pr
where p.rfc=e.rfc and pr.numero=e.numero and denominacion='Infonavit Durango' and fecha between '1/1/2000' and '31/12/2000' 
group by p.rfc, razonsocial
having sum(cantidad) > (select sum(cantidad) 
	from proveedores p, entregan e, proyectos pr
	where p.rfc= e.rfc and pr.numero=e.numero
	and pr.Denominacion='Infonavit Durango' and fecha between '01/01/2001' and '31/12/2001')
*/




/* INSTRUCCIONES

Materiales(Clave, Descripción, Costo, PorcentajeImpuesto) 
Proveedores(RFC, RazonSocial) 
Proyectos(Numero, Denominacion) 
Entregan(Clave, RFC, Numero, Fecha, Cantidad) 

code La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. 

	select sum(cantidad) as 'TotalEntregas', sum(costo)*porcentajeimpuesto as 'ImporteconImpuestos'
	from materiales m, entregan e
	where m.clave=e.clave and fecha between '01/01/1997' and '31/12/1997'


code Para cada proveedor, obtener la razón social del proveedor, número de entregas e importe total de las entregas realizadas. 

code Por cada material obtener la clave y descripción del material, la cantidad total entregada, la mínima cantidad entregada, la máxima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400. 

code Para cada proveedor, indicar su razón social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripción del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500. 

code Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.

*/