
Enrique Posada Lozano		A01700711

Lab18_Muestras


	Primera Parte:
1.
546,82.032

2.
Alvin,5390,794295.2064
Cecoferre,6039,860441.977
Comex,4895,801930.598
La Ferre,7140,919531.43
La fragua,5565,2258223.692
Oviedo,7760,1059888.737
Tabiquera del centro,6042,1937798.724
Tubasa,5005,2278398.993


3.
1010,Varilla 4/32,1718,523,667,201560.914
1040,Varilla 3/18,1349,263,546,220329.472
1050,Varilla 4/34,1216,90,623,217268.8
1080,Ladrillos rojos,1214,86,699,62011.12
1100,Block,1688,466,699,51754.08
1130,Sillar gris,1298,63,673,146006.828
1140,Cantera blanca,1453,219,651,297225.68
1200,Recubrimiento P1019,1415,177,653,347750.4
1220,Recubrimiento P1037,1335,24,658,382920.72
1250,Grava,1452,71,691,148830
1270,Tezontle,1376,324,546,112876.032
1320,Tubería 4.4,1274,163,698,300755.728
1330,Tubería 3.7,1205,93,558,296892.72
1410,Pintura B1021,1529,461,601,196514.725
1420,Pintura C1012,1325,278,603,170328.75


4. 
Oviedo,572.666666,1010,Varilla 4/32
La Ferre,562.666666,1100,Block
Oviedo,509.666666,1410,Pintura B1021


5. 
Comex,197.333333,1210,Recubrimiento P1028
Cecoferre,212,1030,Varilla 4/33
Alvin,222,1360,Pintura C1010
Tubasa,244.666666,1310,Tubería 3.6
Comex,249,1290,Tubería 3.5
Oviedo,250,1170,Cantera amarilla
Cecoferre,287.666666,1350,Tubería 3.8
Alvin,295.333333,1280,Tepetate
La fragua,313,1400,Pintura C1011
La Ferre,319,1180,Recubrimiento P1001
Tubasa,319,1230,Cemento 
Cecoferre,332.333333,1110,Megablock
La Ferre,336.333333,1340,Tubería 4.5
Tubasa,340.333333,1070,Varilla 4/35
Comex,347.333333,1370,Pintura B1020
Tabiquera del centro,354.333333,1060,Varilla 3/19
La fragua,355.666666,1240,Arena
La Ferre,356,1020,Varilla 3/17
La fragua,357,1160,Cantera rosa
Cecoferre,357.333333,1190,Recubrimiento P1010
Alvin,358,1120,Sillar rosa
La Ferre,364.333333,1260,Gravilla
Tabiquera del centro,364.666666,1380,Pintura C1011
Cecoferre,365,1430,Pintura B1022
Tabiquera del centro,365.666666,1300,Tubería 4.3
Oviedo,368.666666,1090,Ladrillos grises
Cecoferre,458.666666,1270,Tezontle
Alvin,471.666666,1200,Recubrimiento P1019
Oviedo,484,1250,Grava
Tabiquera del centro,484.333333,1140,Cantera blanca
Oviedo,509.666666,1410,Pintura B1021
La Ferre,562.666666,1100,Block
Oviedo,572.666666,1010,Varilla 4/32


	Segunda Parte:
1. 
1000,Varilla 3/16

2.
La fragua

3.
Varilla 3/16
Varilla 4/32
Varilla 3/17
Varilla 4/33
Varilla 4/34
Varilla 3/19
Varilla 4/35
Ladrillos rojos
Ladrillos grises
Block
Megablock
Sillar rosa
Sillar gris
Cantera blanca
Cantera rosa
Cantera amarilla
Recubrimiento P1001
Recubrimiento P1010
Recubrimiento P1028
Recubrimiento P1037
Cemento 
Arena
Grava
Gravilla
Tezontle
Tepetate
Tubería 3.5
Tubería 4.3
Tubería 3.6
Tubería 4.4
Tubería 3.7
Tubería 4.5
Pintura C1010
Pintura B1020
Pintura C1011
Pintura B1021
Pintura C1011
Pintura B1021
Pintura C1012
Pintura B1022

4.
NO HAY

5. 
NO HAY
