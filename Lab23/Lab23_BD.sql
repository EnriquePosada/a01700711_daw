
/*--Enrique Posada Lozano 	Seccion de Preguntas BD Lab23
1. ¿Que pasa cuando deseas realizar esta consulta? 
	Visualiza los valores insertados

2. ¿Qué pasa cuando deseas realizar esta consulta? 
	En una de las sesiones si funciona (donde se comenzo la transaccion) pero en la otra sesion no funciona.

3. Explica por qué ocurre dicho evento. 
	Si funciona.

4. ¿Qué ocurrió y por qué? 
	El commit se usa para confirmar lo que se hace en una transaccion, el rollback por otra parte lo deshace, 	por lo que deshace lo que se hizo en la prueba mencionada.

5. ¿Para qué sirve el comando @@ERROR revisa la ayuda en línea? 
	@@error devuelve el numero de error que se obtuvo en la sentencia de SQL.

6. ¿Explica qué hace la transacción? 
	Lo que sucede es que trata de insertar una valores en una cuenta que ya existe la cual viene siendo la 	cuenta '001'.

7. ¿Hubo alguna modificación en la tabla? Explica qué pasó y por qué. 
	No hubo ningun cambio realizado en la tabla porque debido a que hubo un error, no se realizo un commit y por 	ello lo que se hizo fue hacer un rollback que pueda deshacer lo que no se pudo insertar.
*/


--Script de SQL

CREATE TABLE Clientes_Banca 
( 
  NoCuenta varchar(5) not null PRIMARY KEY, 
  Nombre varchar(30), 
  Saldo numeric (10,2) 
)




CREATE TABLE Tipos_Movimiento
( 
  ClaveM varchar(2) not null PRIMARY KEY, 
  Descripcion varchar(30), 
  
)


--Realizan
CREATE TABLE Movimientos
( 
  NoCuenta varchar(5) not null, 
  ClaveM varchar(2) not null, 
  Fecha datetime, 
  Monto numeric (10,2),
  CONSTRAINT FK_CB_M FOREIGN KEY (NoCuenta) REFERENCES Clientes_Banca(NoCuenta),
  CONSTRAINT FK_TM_M FOREIGN KEY (ClaveM) REFERENCES Tipos_Movimiento(ClaveM)
)



BEGIN TRANSACTION PRUEBA1
INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1

select * from Clientes_Banca

BEGIN TRANSACTION PRUEBA2
INSERT INTO Clientes_Banca VALUES('004','Ricardo Rios Maldonado',19000); 
INSERT INTO Clientes_Banca VALUES('005','Pablo Ortiz Arana',15000); 
INSERT INTO Clientes_Banca VALUES('006','Luis Manuel Alvarado',18000); 
COMMIT TRANSACTION PRUEBA2

--Desde la Segunda Sesion
SELECT * FROM Clientes_Banca where NoCuenta='001' 

--Desde la Primera Sesion
ROLLBACK TRANSACTION PRUEBA2 

--Desde la Segunda Sesion
SELECT * FROM Clientes_Banca 

BEGIN TRANSACTION PRUEBA3
INSERT INTO Tipos_Movimiento VALUES('A','Retiro Cajero Automatico');
INSERT INTO Tipos_Movimiento VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3

BEGIN TRANSACTION PRUEBA4
INSERT INTO Movimientos VALUES('001','A',GETDATE(),500);
UPDATE Clientes_Banca SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4


BEGIN TRANSACTION PRUEBA5
INSERT INTO Clientes_Banca VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO Clientes_Banca VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO Clientes_Banca VALUES('001','Oscar Perez Alvarado',8000);


IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END


SELECT * FROM Clientes_Banca

/*--Enrique Posada Lozano 	Seccion de Preguntas BD Lab23
1. ¿Que pasa cuando deseas realizar esta consulta? 
	Visualiza los valores insertados

2. ¿Qué pasa cuando deseas realizar esta consulta? 
	En una de las sesiones si funciona (donde se comenzo la transaccion) pero en la otra sesion no funciona.

3. Explica por qué ocurre dicho evento. 
	Si funciona.

4. ¿Qué ocurrió y por qué? 
	El commit se usa para confirmar lo que se hace en una transaccion, el rollback por otra parte lo deshace, 	por lo que deshace lo que se hizo en la prueba mencionada.

5. ¿Para qué sirve el comando @@ERROR revisa la ayuda en línea? 
	@@error devuelve el numero de error que se obtuvo en la sentencia de SQL.

6. ¿Explica qué hace la transacción? 
	Lo que sucede es que trata de insertar una valores en una cuenta que ya existe la cual viene siendo la 	cuenta '001'.

7. ¿Hubo alguna modificación en la tabla? Explica qué pasó y por qué. 
	No hubo ningun cambio realizado en la tabla porque debido a que hubo un error, no se realizo un commit y por 	ello lo que se hizo fue hacer un rollback que pueda deshacer lo que no se pudo insertar.
*/



