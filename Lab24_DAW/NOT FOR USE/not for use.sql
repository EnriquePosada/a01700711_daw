-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 24, 2018 at 02:10 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Examen2doDAW`
--

-- --------------------------------------------------------

--
-- Table structure for table `DHARMA`
--

CREATE DATABASE IF NOT EXISTS `Examen2doDAW` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Examen2doDAW`;


CREATE TABLE `DHARMA` (
  `UserInput` varchar(50) NOT NULL,
  `HoraIngreso` time NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DHARMA`
--

INSERT INTO `DHARMA` (`UserInput`, `HoraIngreso`, `Status`) VALUES
('4 8 15 16 23 42', '21:01:12', 'SUCCESS'),
('4 8 15 16 23 42', '21:01:16', 'SUCCESS'),
('4 8 15 16 23 42', '21:02:26', 'SUCCESS'),
(' 7 89 89 89 89', '21:03:18', 'FAILURE'),
('1 1 1 1 1 1 1 1', '21:31:31', 'FAILURE'),
('4 8 15 16 23 42', '21:50:11', 'SUCCESS'),
('2 2 222222 ', '21:52:31', 'FAILURE'),
('1212121212', '23:20:19', 'FAILURE'),
('1 1 1 1 1 2 2 3 3 3 ', '00:47:11', 'FAILURE'),
('12', '00:47:37', 'FAILURE'),
('8912891289', '00:47:43', 'FAILURE'),
('12345678900000', '00:55:26', 'FAILURE'),
('123232323232323', '00:59:40', 'FAILURE'),
('', '01:10:19', 'FAILURE');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
