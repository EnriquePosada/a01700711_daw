-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 03, 2018 at 04:53 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Lab14`
--

-- --------------------------------------------------------

--
-- Table structure for table `Lab14_DAW_Frutas`
--

CREATE TABLE `Lab14_DAW_Frutas` (
  `Name` varchar(50) NOT NULL,
  `Units` varchar(5) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Lab14_DAW_Frutas`
--

INSERT INTO `Lab14_DAW_Frutas` (`Name`, `Units`, `Quantity`, `Price`, `Country`) VALUES
('Avocados', 'kg', 50, '20', 'Mexico'),
('Banana', 'kg', 80, '15', 'India'),
('Raspberries', 'kg', 20, '17', 'Russia'),
('Blueberries', 'kg', 30, '50', 'United States'),
('Tomatoes', 'kg', 120, '12', 'Mexico');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
