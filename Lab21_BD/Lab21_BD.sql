

--ENRIQUE POSADA LOZANO				A01700711

/*IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO
            
            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO
*/

--EXECUTE creaMaterial 5000,'Martillos Acme',250,15 

--select * from materiales where clave = 5000

/*IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaMaterial' AND type = 'P')
                DROP PROCEDURE modificaMaterial
            GO
            
            CREATE PROCEDURE modificaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                update Materiales set clave = @uclave, descripcion = @udescripcion, costo = @ucosto, 						porcentajeimpuesto = @uimpuesto 
            GO
*/

/*IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaMaterial' AND type = 'P')
                DROP PROCEDURE eliminaMaterial
            GO
            
            CREATE PROCEDURE eliminaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                delete from Materiales where clave = @uclave
            GO
*/


/*IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
                            GO
*/


/*IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO
            
            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO

--EXECUTE creaMaterial 5000,'Martillos Acme',250,15 

--select * from materiales where clave = 5000




--Para Proveedores  (CAMBIAR INFO)

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProveedor' AND type = 'P')
                DROP PROCEDURE creaProveedor
            GO
            
            CREATE PROCEDURE creaProveedor
					@urfc varchar(50),
					@urazonsocial varchar(50)
            AS
                INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProveedor' AND type = 'P')
                DROP PROCEDURE modificaProveedor
            GO
            
            CREATE PROCEDURE modificaProveedor
					@urfc varchar(50),
					@urazonsocial varchar(50) 

            AS
                update Proveedores set rfc=@urfc, razonsocial=@urazonsocial
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaProveedor' AND type = 'P')
                DROP PROCEDURE eliminaProveedor
            GO
            
            CREATE PROCEDURE eliminaProveedor
              @urfc varchar(50),
				@urazonsocial varchar(50)

            AS
                delete from Proveedores where rfc = @urfc
            GO


--Para PROYECTOS  (CAMBIAR INFO)

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProyecto' AND type = 'P')
                DROP PROCEDURE creaProyecto
            GO
            
            CREATE PROCEDURE creaProyecto
              @unumero numeric(5), 
  				@udenominacion varchar(50) 

            AS
                INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProyecto' AND type = 'P')
                DROP PROCEDURE modificaProyecto
            GO
            
            CREATE PROCEDURE modificaProyecto
                @unumero numeric(5), 
  				@udenominacion varchar(50)
            AS
                update Proyectos set numero=@unumero, denominacion=@udenominacion
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaProyecto' AND type = 'P')
                DROP PROCEDURE eliminaProyecto
            GO
            
            CREATE PROCEDURE eliminaProyectos
                  @unumero numeric(5), 
  					@udenominacion varchar(50)
            AS
                delete from Proyectos where numero = @unumero
            GO




--Para ENTREGAN  (CAMBIAR INFO)

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaEntregan' AND type = 'P')
                DROP PROCEDURE creaEntregan
            GO
            
            CREATE PROCEDURE creaEntregan
					  @uclave numeric(5), 
					  @urfc char(13), 
					  @unumero numeric(5), 
					  @ufecha DateTime, 
					  @ucantidad numeric (8,2)
            AS
                INSERT INTO Entregan VALUES(@uclave, @urfc, @unumero, @ufecha, @ucantidad)
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaEntregan' AND type = 'P')
                DROP PROCEDURE modificaEntregan
            GO
            
            CREATE PROCEDURE modificaEntregan
                	  @uclave numeric(5), 
					  @urfc char(13), 
					  @unumero numeric(5), 
					  @ufecha DateTime, 
					  @ucantidad numeric (8,2)
            AS
                update Entregan set clave = @uclave, rfc = @urfc, numero = @unumero, 						fecha = @ufecha, cantidad=@ucantidad 
            GO


IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaEntregan' AND type = 'P')
                DROP PROCEDURE eliminaEntregan
            GO
            
            CREATE PROCEDURE eliminaEntregan
                    @uclave numeric(5), 
					  @urfc char(13), 
					  @unumero numeric(5), 
					  @ufecha DateTime, 
					  @ucantidad numeric (8,2)
            AS
                delete from Entregan where clave = @uclave
            GO



/*
--PROCESO DE PROYECTO

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaUsuario' AND type = 'P')
                DROP PROCEDURE creaUsuario
            GO
            
            CREATE PROCEDURE creaUsuario
                @uidusuario NUMERIC(5),
                @uusername VARCHAR(30),
                @unombre VARCHAR(19),
                @upassword VARCHAR(1024),
				 @utelefono NUMERIC(11),
				 @uapellidopaterno VARCHAR(11),
				 @uapellidomaterno VARCHAR(9),
				 @ufirma VARCHAR(1),
				 @ustatus NUMERIC(1)
            AS
                INSERT INTO Usuario VALUES(@uidusuario, @uusername, @unombre, @upassword, @utelefono, @uapellidopaterno, @uapellidomaterno, @ufirma, @ustatus )
            GO


*/



/*
1. ¿Qué hace el primer bloque del código (bloque del IF)? 
	Las sentencias permiten que cuando se ejecute crea material, se pueda insertar registros de forma mas 	rapida y sencilla.

2. ¿Para qué sirve la instrucción GO? 
	Para ejecutar la instruccion anterior. Este senala el fin de un batch de sentencias SQL.

3. ¿Explica que recibe como parámetro este Procedimiento y qué tabla modifica?
		Pues va a recibir de la tabla materiales la clave, descripcion, costo y el porcentajeimpuesto.


4. Explica en tu reporte quérecibe como parámetro este Procedimiento y qué hace (EXECUTE queryMaterial 'Lad',20 )
	El procedimiento queryMaterial simplemente busca los materiales cuya descripcion sea la misma que fue 	insertada y el costo sea mayor al costo introducido.

1080,Ladrillos rojos,50,2.16
1090,Ladrillos grises,35,2.18


5. ¿Qué ventajas tienen el utilizar Store Procedures en una aplicación cliente-servidor? 
	Stored Procedures son utiles para simplificar el proceso realizado para realizar una consulta. Posiblemente 	cuando una consulta es muy larga, es mas conveniente y sencillo simplicar todo el proceso a que se haga con 	solo introducir una linea de codigo. 

6. ¿Qué ventajas tiene utilizar SP(Stored Procedure) en un proyecto?
	Como se menciono en la pregunta anterior, usar stored procedures simplifica las consultas de forma que 	solamente se tenga que correr una linea de codigo para hacer una consulta sin tener que escribir mucho. 

*/



