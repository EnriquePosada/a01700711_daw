<!DOCTYPE html>
<html>
<head>
	<title>Lab #11: Manejo de Formas con PHP</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<div class="jumbotron">
	<h1>Lab #11: Manejo de Formas con <i>PHP</i> y Modelo de Capas</h1>
	</div>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<style type="text/css">
	.error{
		color: red;
	}
	.forma{
		text-align: center;
	}
</style>
<?php include("Lab11_PHP.php"); ?>
<body>
	<div class="forma">
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

		<label>Nombre Usuario: </label><br><input type="text" name="user" >
		<span class="error">* <?php echo $usernameErr;?></span>
		<br><br>
		<label> Contraseña:</label><br>
		<input type="password" name="password" >
		<span class="error">* <?php echo $passwordErr;?></span>
		<br><br>
		<button type="submit" class="btn btn-secondary" data-toggle="modal" data-target="">Iniciar <i>Sesion</i></button>	

	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Crear <i>Cuenta</i></button>

	</form>
	</div>

	<label>*PUBLIC_NOTE: El error de los required fields se debe al <i>refresh</i>. Es por ello que de momento debes de volver a entrar a la pagina.</label>



	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Hola, Crea tu Cuenta</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">


	        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

	          <div class="form-group">
	            <label for="recipient-name" class="form-control-label">Nombre Usuario</label>
	            <input type="text" class="form-control" id="recipient-name" name="username"> <span class="error"> * <?php echo $usernameErr;?></span>
	          </div>
	          <div class="form-group">
	          <label>Contraseña</label><br>
	            <input type="password" for="message-text" class="form-control-label" id="passwd" name="userpassword">
	            <span class="error"> * <?php echo $passwordErr; ?> </span>
	          </div>
	          <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary" onclick="bienvenido()">Crear Cuenta</button>
	      </div>
	        </form>


	      </div>
	      
	    </div>
	  </div>
	</div>




		
	<div id="accordion">
	  	<div class="card">
	    	<div class="card-header" id="headingOne">
	     		 <h5 class="mb-0">
	        		<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">¿Por qué es una buena práctica separar el controlador de la vista?</button></h5>
	</div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
			<ul>
				<li>La razon por la cual es una buena practica el tener que separar el controlador de la vista es porque permite facilitar la funcionalidad del sistema.</li>
			</ul>
			</div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">

		<ul>
		<li><b>$_SERVER</b>
		<ul>
			<li>$_SERVER es un array que contiene información, tales como cabeceras, rutas y ubicaciones de script. Las entradas de este array son creadas por el servidor web.</li>
		</ul>
		</li>
		<li><b>$_REQUEST</b>
		<ul>
			<li>$_REQUEST es un array asociativo que por defecto contiene el contenido de $_GET, $_POST y $_COOKIE.</li>
		</ul>
		</li>
		<li><b>$_SESSION</b>
		<ul>
			<li>$_SESSION es un array asociativo el cual contiene las variables de sesión que se encuentran disponibles para el <b>script actual</b>.</li>
		</ul>
		</li>
		<li><b>$_COOKIE <i>(Extra debido a que lo encontre bajo la informacion de $_REQUEST)</i></b>
		<ul>
			<li>$_COOKIE es una variable tipo array asociativo de variables pasadas al script actual a través de Cookies HTTP.</li>
		</ul>
		</li>
			</ul>
      
</div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
	<ul>
		<li><i>password_hash()</i>
		<ul>
			<li>De acuerdo a la pagina de <i>php</i>, <i>password_hash()</i> crea un nuevo hash de contraseña usando un algoritmo de hash fuerte de único sentido. Ademas, parece ser que cada vez que se refresca la pagina el cifrado cambia para el contenido que se desea encriptar.</li>
		</ul>
		</li>
			<li><i>password_verify()</i>
		<ul>
			<li>La funcion de <i>password_verify()</i> se encarga de hacer lo opuesto que hace <i>password_hash()</i> ya que esta funcion revisa que la contraseña que se inserta coincida con la contraseña que habia sido encriptada y de pasar la prueba significa que ambas contraseñas coinciden.</li>
		</ul>
		</li>
		<li><b>NOTA PERSONAL</b>: Existen otras funciones que son llamadas <i>crack</i> que permiten probar la resistencia que tienen las contraseñas para que no puedan ser "crackeadas".</li>
		<li><i>*Si desean ver como sirven estas <b>funciones</b>, pueden ver una prueba en el laboratorio de introduccion a PHP(Lab#9 de mi cuenta). Esperaria volver a usar estas funciones una vez que se incorporen bases de datos o en manejo de sesiones.</i></li>
	</ul>      
	</div>
    </div>
  </div>
</div>
<div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          ¿Qué es XSS y cómo se puede prevenir?
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
	<ul>
		<li><i>XSS</i> viene representando lo que es <i>Cross Site Scripting</i> y esto basicamente consiste en ataques por medio de inyeccion de codigo. Es por medio de estos ataques que puedes alterar toda la estructura de la pagina web. </li>
		<li>Es por ello que para prevenir estos ataques es necesario usar funciones como <i>htmlspecialchars()</i>que eliminen los tags como < > para que aquello que se escribe no pueda servir en lo absoluto.</li>
			
	</ul>      
	</div>
    </div>
  </div>
</div>

<br><br>

</body>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/javascript">
			
			function bienvenido() {
			var name=document.getElementById('recipient-name');
			var passwd=document.getElementById('passwd');
			if (passwd.value=="" && name.value=="") {
				alert("Sugiero que escribas un nombre de usuario y una contraseña");
			}
			else if (passwd.value=="") {
					alert("Te sugiero que escribas tu Contraseña");
				}
			else{
					alert("Te sugiero que escribas tu nombre de Usuario");
				}
			}
		
	</script>

</html>