<?php 

	//session_start();

	$timeout = 6480;

    require_once("../util.php");


    if(isset($_SESSION["timeout"]) ) {
    	$duration = time() - (int)$_SESSION['timeout'];
        include("../partials/header.html");
        include("../partials/body.html");
        include("../partials/footer.html");
        if ($duration>$timeout) {
        	session_destroy();
            insertReporte("","FAILURE");
        	header("location:../sesioncaducada.php");
        }
    } else {
        include("../partials/header.html");
        include("../partials/body.html");
        include("../partials/footer.html");
    }

	$_SESSION['timeout'] = time();

 ?>