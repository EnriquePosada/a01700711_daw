<?php 

	function connectDataBase(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "Lab16";

	$conexion = mysqli_connect($servername, $username, $password, $dbname);


	//Revisar la conexion 
	if(!$conexion){
		die("Connection Failed: " .mysqli_connect_error());
	}
	return $conexion;
	
	}

	function closeDataBase($mysql){
		mysqli_close($mysql);
	}

	function getUsers(){
		$connection = connectDataBase();
		$sql = "SELECT UserName, Password, Country FROM Lab16_Users";

		$result=mysqli_query($connection, $sql);
		closeDataBase($connection);

		return $result;
	}

	function getUsersbyName($User_name){
		$connection=connectDataBase();
		$sql="SELECT UserName, Password, Country FROM Lab16_Users WHERE UserName LIKE '%".$User_name."%'";
		$result=mysqli_query($connection, $sql);
		closeDataBase($connection);
		return $result;
	}

	function insertUser($username, $password){
		 $db = connectDataBase();
        if ($db != NULL) {
            
            // insert command specification 
            $query='INSERT INTO Lab16_Users (UserName, Password, Country) VALUES (?,?, "Mexico") ';
            //Los '  ?  ' representan algo que sera reemplazado

            //$sql="INSERT INTO Lab16_Users (UserName, Password, Country) VALUES (\"".$username."\",\"".$password.",".$country."\");"; //Este codigo es una porqueria que permite SQL injection
            
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("ss", $username, $password)) {//string string... si es ssi entonces seria string string int
            	//Aqui reemplzada los signos de interrogacion
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
             // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              } 

            mysqli_free_result($result);
            closeDataBase($db);
            return true;
        } 
        return false;
	}


	function login($user, $passwd) {
        $db = connectDataBase();
        if ($db != NULL) {
            
            //Specification of the SQL query
           // $query= "SELECT *  FROM Lab16_Users WHERE UserName LIKE ? AND Password LIKE ? ";
            $query='SELECT * FROM Lab16_Users WHERE UserName="'.$user.
                '" AND Password="'.$passwd.'"';
             //echo $query;
             //die();
             // Query execution; returns identifier of the result group
            $result = $db->query($query);
             // cycle to explode every line of the results
            //Specification of the SQL query
            //echo $result;
            //die();
            if (mysqli_num_rows($result) > 0)  {
                // it releases the associated results
                mysqli_free_result($result);
                closeDataBase($db);
                return true;
            }
            return false;
        } 
        return false;
            // Preparing the statement 
            /*if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("ss", $username, $password)) {//string string... si es ssi entonces seria string string int
            	//Aqui reemplzada los signos de interrogacion
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
             // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              } 

            if (mysqli_num_rows($result) > 0)  {
                // it releases the associated results
                mysqli_free_result($result);
                closeDataBase($db);
                return true;
            }
            return false;
        } 
        return false;*/
    }


	function removeUser($User_name){
		$connection=connectDataBase();
		$sql="DELETE FROM Lab16_Users WHERE UserName = '".$User_name."'";
		$result=mysqli_query($connection, $sql);
		closeDataBase($connection);
		return $result;
	}

	/*function getCheapestFruits($cheap_price){
		$connection=connectDataBase();
		$sql="SELECT Name, Units, Quantity, Price, Country FROM Lab16_Users WHERE price <='".$cheap_price."'";
		$result=mysqli_query($connection, $sql);
		closeDataBase($connection);
		return $result;

	}*/

 ?>